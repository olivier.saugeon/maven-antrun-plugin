<!---
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->

# Maven AntRun Plugin

This is a fork of https://github.com/apache/maven-antrun-plugin

## Changes

I made this fork because I needed more recent Ant features for another project.

Changes to the original project are the following:
- upgrade to Ant 1.10.7
- include Ant-XZ

## Updates

I try to follow the `master` branch of original project as much as possible.
Latest merge of `master` into `ant-1.10.7` branch on _2020-06-05_.